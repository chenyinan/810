package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User row);

    User selectByPrimaryKey(Integer id);

    List<User> selectAll();

    int updateByPrimaryKey(User row);


    Integer delRole(@Param("id") Integer id);

    Integer delUser(@Param("id") Integer id);

}