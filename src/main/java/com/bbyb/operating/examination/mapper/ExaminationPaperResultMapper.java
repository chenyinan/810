package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.ExaminationPaperResult;
import java.util.List;

public interface ExaminationPaperResultMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationPaperResult row);

    ExaminationPaperResult selectByPrimaryKey(Integer id);

    List<ExaminationPaperResult> selectAll();

    int updateByPrimaryKey(ExaminationPaperResult row);
}