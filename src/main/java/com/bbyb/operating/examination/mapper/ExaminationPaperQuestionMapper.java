package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.ExaminationPaperQuestion;
import java.util.List;

public interface ExaminationPaperQuestionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationPaperQuestion row);

    ExaminationPaperQuestion selectByPrimaryKey(Integer id);

    List<ExaminationPaperQuestion> selectAll();

    int updateByPrimaryKey(ExaminationPaperQuestion row);
}