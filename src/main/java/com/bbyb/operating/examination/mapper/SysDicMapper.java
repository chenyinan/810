package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.SysDic;
import java.util.List;

public interface SysDicMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysDic row);

    SysDic selectByPrimaryKey(Integer id);

    List<SysDic> selectAll();

    int updateByPrimaryKey(SysDic row);
}