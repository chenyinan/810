package com.bbyb.operating.examination.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiError {
    private HttpStatus status;  // 错误状态码
    private String message;     // 错误消息
    private List<String> errors; // 错误详情
}
