package com.bbyb.operating.examination.model.po;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class User {
    private Integer id;



    private String userCode;


    private String userName;

    private String phone;

    private String password;

    private String email;

    private String idCard;

    private Integer gender;

    private Integer isAdmin;

    private Integer isEnable;

    private String createBy;

    private LocalDateTime createTime;

    private String updateBy;

    private LocalDateTime updateTime;

    private Integer invalid;


}