package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.handler.GlobalExceptionHandler;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.po.UserTest;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

/**
 * 账号
 * className: UserController
 * datetime: 2023/2/10 14:28
 * author: lx
 */

@Api(tags = "用户管理")
@RestController()
@RequestMapping("/user")
public class UserController {



/*    @Autowired
    private Exception exception;*/

    @Autowired
    private UserService userService;

    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @ApiOperation("用户添加")
    @PostMapping(value = "add")
    public CommonResult<Boolean> addUser(@RequestBody User user){
        if(user == null){
            return new CommonResult<>(601, "用户信息不能为空");
        }

        if(StringUtils.isBlank(user.getUserCode())){
            return new CommonResult<>(601, "用户编码不能为空");
        }

        if(StringUtils.isBlank(user.getUserName())){
            return new CommonResult<>(601, "用户名不能为空");
        }

        if(StringUtils.isBlank(user.getPhone())){
            return new CommonResult<>(601, "用户手机号不能为空");
        }

        if(StringUtils.isBlank(user.getPassword())){
            return new CommonResult<>(601, "用户密码不能为空");
        }

        String errorMsg = userService.addUser(user);
        if(StringUtils.isNotEmpty(errorMsg)){
            return new CommonResult<>(603, errorMsg);
        }
        return new CommonResult<>(true);
    }

    @ApiOperation("用户查询")
    @PostMapping("/allUser")
    public List<User> getAllUser(){
        return userService.getAllUser();
    }


    @PostMapping("/find/{id}")
    public CommonResult<Boolean> find(@PathVariable Integer id){
      String errMag=  userService.delUser(id);

      if (StringUtils.isNotEmpty(errMag)){
          return new CommonResult<>(603,errMag);
      }else {
          return new CommonResult<>(true);
      }

    }

    @PostMapping("/testUser")
    public CommonResult<Boolean> testUser(@Valid @RequestBody UserTest userTest){

     /*   String message = exception.getMessage();

        System.out.println(message);*/

        Set<ConstraintViolation<UserTest>> validate = validator.validate(userTest);

        if (!validate.isEmpty()) {
            for (ConstraintViolation<UserTest> violation : validate) {
                return new CommonResult<>(601, violation.getMessage());
            }
        } else {
            System.out.println("校验通过");
        }


        if(StringUtils.isBlank(userTest.getUserCode())){
            return new CommonResult<>(601, "用户编码不能为空");
        }

        if(StringUtils.isBlank(userTest.getUserName())){
            return new CommonResult<>(601, "用户名不能为空");
        }

        return new CommonResult<>(true);
    }






}



